# DB SIGNIN DETAILS
DB_USERNAME = 'root'
DB_PASSWORD = 'rootpassword'
DB_IP = '35.187.183.86'
DB_PORT = '3306'

# DB  DETAILS
DB_DATABASE = 'dev3_ase_scm'
DB_DATABASE_USER='ase_scm_cntl'
DB_STAGE_DATABASE='dev2_ase_scm'

DUBLIN_AIR_SENSOR_DUBLIN_STAGE_TABLE='stg_irl_dub_air_snsr_'
DUBLIN_BIKES_TABLE = 'dim_irl_dub_bks'
DUBLIN_DART_STATION_TABLE = 'dim_irl_dart_stn_rstr'
DUBLIN_DART_STATION_SCHEDULE_TABLE = 'dim_irl_dart_stn_schd'
DUBLIN_FOOTFALL_TABLE = 'dim2_irl_dub_ppl_ftfl'
DUBLIN_LUAS_STATION_LIST_TABLE = 'dim_irl_dub_luas_rstr'
DUBLIN_AIR_SENSOR_LIST_TABLE='dim_irl_dub_air_snsr_rstr'
DUBLIN_AIR_SENSOR_TABLE_TAG='dim_irl_dub_air_snsr_'
DUBLIN_TRAFFIC_DENSITY_TABLE = 'dim_irl_dub_trfc_cams'
USER_DETAILS="usr_dtl"
DUBLIN_REROUTE_BUS_SHAPES = "dim_irl_dub_gtfs_bus_shapes"
DUBLIN_REROUTE_BUS_TRIPS = "dim_irl_dub_gtfs_bus_trips_unique"

DB_CONNECT_URL = 'mysql+mysqlconnector://'+DB_USERNAME+':'+DB_PASSWORD+'@'+DB_IP+':'+DB_PORT+'/'+DB_DATABASE
DB_CONNECT_URL_USER_DATABASE='mysql+mysqlconnector://'+DB_USERNAME+':'+DB_PASSWORD+'@'+DB_IP+':'+DB_PORT+'/'+DB_DATABASE_USER


# ML Model Paths
ML_ENGINE_PICKLE_PATH = './Resources/Dublin_Bike_ML_Model.pkl'
ML_AIR_POLLUTION_PICKLE_PATH='./Resources/ml_engine_air/Models/'
CSV_PATH='./Resources/ml_engine_air' \
         '/ProcessedData/'

# API END POINTS
SLASH = "/"
USER_DATA_MODEL="user"
BIKES_ENDPOINT_STRING = "bikesdata"
DART_STATIONS_ENDPOINT_STRING = "dartstations"
DART_STATIONS_SCHEDULE_ENDPOINT_STRING = "dartstationsschedule"
FOOTFALL_ENDPOINT_STRING = "footfall"
LUAS_STATIONS_ENDPOINT_STRING = "luasstationlist"
TRAFFIC_DENSITY_ENDPOINT_STRING = "trafficdensity"
PREDICTED_AVAILABLE_BIKES = "predictavailablebikes"
DART_REALTIME_LOCATION = "dartrealtimelocation"
LUAS_REAL_TIME_STATION_STATUS = "luasstationstatus"
LUAS_STATION_URL_PARAM = "station"
REROUTE_DEVICEID_URL_PARAM = "deviceid"
GET_ALL_USER_ENDPOINT_STRING="user"
USER_LOGIN_ENDPOINT_STRING="login"
USER_LOGOUT_ENDPOINT_STRING="logout"
USER_SIGNUP_ENDPOINT_STRING="signup"
USER_FORGOT_PASSWORD_ENDPOINT_STRING="forgotpassword"
GET_AIR_SENSOR_DATA_STRING= "getairsensordata"
GET_AIR_SENSOR_PREDICT_ENDPOINT_STRING="getpredictedvalue"
GET_ALL_AIR_SENSOR_DETAILS_ENDPOINT_STRING="getallsensordetails"
BUS_SHAPES_ENDPOINT_STRING = "busshapes"
REROUTE_DATA_ENDPOINT = "reroute"
REROUTE_POLYGON_ENDPOINT = "reroutepolygon"

# Flask Configuration
JWT_ALGORITHM="HS256"
JWT_SECRET_KEY="sustainableCityMangement"

# External URLS
DB_BIKE_DATA_FETCH = 'https://flask-python-webapp-quickstart-ase.azurewebsites.net/bikesdata'
DART_REAL_TIME_URL="http://api.irishrail.ie/realtime/realtime.asmx/getCurrentTrainsXML_WithTrainType?TrainType=D"
REAL_TIME_WEATHER_URL="http://metwdb-openaccess.ichec.ie/metno-wdb2ts/locationforecast?lat=%s;long=%s"
BUS_REAL_TIME_URL = "https://api.nationaltransport.ie/gtfsr/v2/gtfsr?format=json"
BUS_REAL_TIME_API_KEY = 'ca2a313218704f118eda0ade10f3ee60'

# Bus API Endpoints
BUS_OPTIONS_SRTING = 'busoptions'
BUS_TRIP_ID_STRING = 'bustripids'
BUS_SHAPES_STRING = 'busshapes'

# Bus Tabel Names
BUS_AGENCY_TABLE = 'dim_irl_dub_gtfs_bus_agncy_st'
BUS_CALENDAR_DATES_TABLE = 'dim_irl_dub_gtfs_bus_cdr_dts'
BUS_CALENDAR_TABLE = 'dim_irl_dub_gtfs_bus_clndr'
BUS_ROUTES_TABLE = 'dim_irl_dub_gtfs_bus_routes'
BUS_SHAPES_TABLE = 'dim_irl_dub_gtfs_bus_shapes'
BUS_STOPS_TABLE = 'dim_irl_dub_gtfs_bus_stops'
BUS_STOP_TIMES_TABLE = 'dim_irl_dub_gtfs_bus_stp_tms'
BUS_TRIP_TABLE = 'dim_irl_dub_gtfs_bus_trips'