import json
import unittest

from app import app
from utilities.ApiHandler import ApiHandler
import constants.config as config


class TestApp(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    def test_index_startpage(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.decode(), "Welcome To Our Backend Flask Application")

    endpoints_urls_list = [config.BIKES_ENDPOINT_STRING, config.FOOTFALL_ENDPOINT_STRING,
                           config.DART_STATIONS_ENDPOINT_STRING, config.LUAS_STATIONS_ENDPOINT_STRING]

    def test_get_bikes_data(self):
        response = self.app.get(config.BIKES_ENDPOINT_STRING)
        self.assertEqual(response.status_code, 200)
        # verify that data is non-empty
        data = response.text
        self.assertTrue(data)

    def test_get_dart_stations(self):
        response = self.app.get('/dartstations')
        self.assertEqual(response.status_code, 200)
        # verify that data is non-empty
        data = response.text
        self.assertTrue(data)

    def test_get_dartstationschedulelocation(self):
        response = self.app.get('/dartstationsschedule')
        self.assertEqual(response.status_code, 200)
        # verify that data is non-empty
        data = response.text
        self.assertTrue(data)

    def test_get_footfall_data(self):
        response = self.app.get('/footfall')
        self.assertEqual(response.status_code, 200)
        # verify that data is non-empty
        data = response.text
        self.assertTrue(data)

    def test_get_luas_station_list_data(self):
        response = self.app.get('/luasstationlist')
        self.assertEqual(response.status_code, 200)
        # verify that data is non-empty
        data = response.text
        self.assertTrue(data)

    def test_get_traffic_density(self):
        response = self.app.get('/trafficdensity')
        self.assertEqual(response.status_code, 200)
        # verify that data is non-empty
        data = response.text
        self.assertTrue(data)

    # def test_get_predicted_available_bikes(self):
    # response = self.app.get('/predictavailablebikes')
    # self.assertEqual(response.status_code, 500)
    # verify that data is non-empty
    # data = response.text
    # self.assertTrue(data)

    def test_get_dart_realtime_location(self):
        response = self.app.get('/dartrealtimelocation')
        self.assertEqual(response.status_code, 200)
        # verify that data is non-empty
        data = response.text
        self.assertTrue(data)

    def test_get_real_time_luas_station_status(self):
        response = self.app.get('/luasstationstatus')
        self.assertEqual(response.status_code, 200)
        # verify that data is non-empty
        data = response.text
        self.assertTrue(data)

    def test_get_reroute_data(self):
        response = self.app.get('/reroute?deviceid=DCC-AQ69')
        self.assertEqual(response.status_code, 200)
        # verify that data is non-empty
        data = response.text
        self.assertTrue(data)

    def test_get_reroute_polygon(self):
        response = self.app.get('/reroutepolygon')
        self.assertEqual(response.status_code, 200)
        # verify that data is non-empty
        data = response.text
        self.assertTrue(data)

    def test_get_all_sensor_details(self):
        response = self.app.get(config.SLASH + config.GET_ALL_AIR_SENSOR_DETAILS_ENDPOINT_STRING)
        self.assertEqual(response.status_code, 200)
        # verify that data is non-empty
        data = response.text
        self.assertTrue(data)

    def test_get_predicted_value(self):
        response = self.app.get("/getpredictedvalue?sensorname=DCC-AQ22&year=2023&month=03&day=31&hour=12&minute=15")
        self.assertEqual(response.status_code, 200)
        # verify that data is non-empty
        data = response.text
        self.assertTrue(data)

    def test_get_senor_data(self):
        response = self.app.get("/getairsensordata?sensorname=DCC-AQ2")
        self.assertEqual(response.status_code, 200)
        # verify that data is non-empty
        data = response.text
        self.assertTrue(data)

    def test_get_all_user(self):
        response = self.app.get("/user")
        self.assertEqual(response.status_code, 200)

    def test_log_out(self):
        response = self.app.get("/logout")
        self.assertEqual(response.status_code, 200)

    def test_predicted_bike_data(self):
        response = self.app.get("/predictavailablebikes")
        self.assertEqual(response.status_code, 200)
        # verify that data is non-empty
        data = response.text
        self.assertTrue(data)

    def test_get_bus_options(self):
        response = self.app.get(config.SLASH + config.BUS_OPTIONS_SRTING)
        self.assertEqual(response.status_code, 200)
        # verify that data is non-empty
        data = response.text
        self.assertTrue(data)

    def test_send_bus_options(self):
        response = self.app.get(config.SLASH + config.BUS_TRIP_ID_STRING, )
        self.assertEqual(response.status_code, 200)
        # verify that data is non-empty
        data = response.text
        self.assertTrue(data)

    def test_send_shape_stop_json(self):
        response = self.app.get(config.SLASH + config.BUS_SHAPES_STRING)
        self.assertEqual(response.status_code, 200)
        # verify that data is non-empty
        data = response.text
        self.assertTrue(data)

    def test_login(self):
        response = self.app.post('/login', json={"email": "luasmanager@gmail.com", "password": "luas"})
        self.assertEqual(response.status_code, 201)
        data = response.text
        self.assertTrue(data)

    def test_sigup(self):
        response = self.app.post('/signup', json={"name": "LuasManager",
                                                  "email": "luasmanager@gmail.com",
                                                  "password": "luas",
                                                  "role": "user"
                                                  })
        self.assertEqual(response.status_code, 202)
        data = response.text
        self.assertTrue(data)

    def test_get_api_json(self):
        response = ApiHandler.api_get_api_content_json("https://data.smartdublin.ie/dublinbikes-api/last_snapshot/")
        self.assertTrue(response)


if __name__ == '__main__':
    unittest.main()
