from dbhandler.BikesModel import BikesModel
from dbhandler.BusRerouteTripsModel import BusRerouteTripsModel
from dbhandler.DartStationsModel import DartStationsModel
from dbhandler.DartStationsScheduleModel import DartStationsScheduleModel
from dbhandler.FootfallModel import FootfallModel
from dbhandler.LuasStationListModel import LuasStationListModel
from dbhandler.TrafficDensityModel import TrafficDensityModel
from dbhandler.UserModel import UserModel
from dbhandler.BusAgencyModel import BusAgencyModel
from dbhandler.BusCalendarDatesModel import BusCalendarDatesModel
from dbhandler.BusCalendarModel import BusCalendarModel
from dbhandler.BusRoutesModel import BusRoutesModel
from dbhandler.BusStopTimesModel import BusStopTimesModel
from dbhandler.BusTripsModel import BusTripsModel
from dbhandler.BusStopsModel import BusStopsModel
from dbhandler.DbConnection import DbConnection
from dbhandler.BusRerouteShapeModel import BusRerouteShapeModel
import constants.config as config
import pandas as pd
from geopy.geocoders import Nominatim

import mysql.connector
import traceback


class CommonUtil():
    db = DbConnection(config.DB_CONNECT_URL)
    db_user = DbConnection(config.DB_CONNECT_URL_USER_DATABASE)

    def datapull_db_model(self, model_name):
        session = self.db.dbFetchSession()
        if model_name==config.USER_DATA_MODEL:
            session = self.db_user.dbFetchSession()
        query = session.query(self.get_model_object(model_name))
        df = pd.read_sql(query.statement, query.session.bind.connect())
        if model_name == config.FOOTFALL_ENDPOINT_STRING:
            return df
        else:
            return df.to_json(orient='records')


    def get_session_based_on_model(self,model_name):
        session = self.db.dbFetchSession()
        if (model_name == config.USER_DATA_MODEL):
            session = self.db_user.dbFetchSession()
        return session

    def get_model_object(self, model_name):
        if model_name == config.BIKES_ENDPOINT_STRING:
            return BikesModel
        if model_name == config.DART_STATIONS_ENDPOINT_STRING:
            return DartStationsModel
        if model_name == config.DART_STATIONS_SCHEDULE_ENDPOINT_STRING:
            return DartStationsScheduleModel
        if model_name == config.FOOTFALL_ENDPOINT_STRING:
            return FootfallModel
        if model_name == config.LUAS_STATIONS_ENDPOINT_STRING:
            return LuasStationListModel
        if model_name == config.TRAFFIC_DENSITY_ENDPOINT_STRING:
            return TrafficDensityModel
        if model_name == config.USER_DATA_MODEL:
            return UserModel
        if model_name == config.BUS_AGENCY_TABLE:
            return BusAgencyModel
        if model_name == config.BUS_CALENDAR_DATES_TABLE:
            return BusCalendarDatesModel
        if model_name == config.BUS_CALENDAR_TABLE:
            return BusCalendarModel
        if model_name == config.BUS_ROUTES_TABLE:
            return BusRoutesModel
        if model_name == config.BUS_STOP_TIMES_TABLE:
            return BusStopTimesModel
        if model_name == config.BUS_TRIPS_TABLE:
            return BusTripsModel
        if model_name == config.BUS_STOPS_TABLE:
            return BusStopsModel

    def get_lat_long(self, location_name):
        geolocator = Nominatim(user_agent="MyApp")
        try:
            location = geolocator.geocode(location_name.replace("_", " "))
            return str(str(location.latitude) + ", " + str(location.longitude))
        except:
            return str(location_name)


    @staticmethod
    def get_db_data(query, dataBaseName=config.DB_DATABASE):
        try:
            cnx = mysql.connector.connect(user=config.DB_USERNAME, password=config.DB_PASSWORD,
                                          host=config.DB_IP, port=config.DB_PORT,
                                          database=dataBaseName)
            result = pd.read_sql_query(query, cnx)
            cnx.close()
            return result
        except:
            traceback.print_exc()
            return pd.DataFrame()
