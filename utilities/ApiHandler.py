import traceback

import requests
import xmltodict


class ApiHandler():


    @staticmethod
    def api_get_from_xml_dict( url):
        try:
            response = requests.get(url,timeout=5)
            data = xmltodict.parse(response.content)
            return data
        except:
            print("Exception Occured")
            traceback.print_exc()
            return dict()

    @staticmethod
    def api_get_api_content_json(url,header=None):
        try:
            api_response=requests.get(url,headers=header,timeout=5)
            if api_response.status_code == 200 or api_response.status_code ==201:
                return api_response.json()
            else:
                return {}
        except:
            traceback.print_exc()
            return {}



