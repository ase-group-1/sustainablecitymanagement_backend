import unittest
from unittest.mock import MagicMock
from core.FootfallHelper import FootfallHelper
import pandas as pd
class TestFootfallHelper(unittest.TestCase):

    def setUp(self):
        self.footfall_helper = FootfallHelper()

    '''def test_get_footfall_data(self):
        # create mock data
        mock_footfall_df = pd.DataFrame({'time': ['2022-01-01 12:00:00', '2022-01-01 13:00:00'],
                                          '53.3446,-6.2668': [100, 200],
                                          '53.3457,-6.2678': [150, 250]})
        mock_common_util = MagicMock()
        mock_common_util.datapull_db_model.return_value = mock_footfall_df
        FootfallHelper.common_util = mock_common_util

        # run the code
        result = self.footfall_helper.get_footfall_data()

        # check the output
        #expected_result = '[{"lat":"53.3469467","long":"-6.259063","footfall":3184},{"lat":"53.3472205","long":"-6.2606647","footfall":0},{"lat":"53.3340315","long":"-6.245187","footfall":123},{"lat":"53.3336122","long":"-6.244432","footfall":6623},{"lat":"-37.8026645","long":"144.9557819","footfall":2495},{"lat":"53.34494875","long":"-6.260203259102926","footfall":168},{"lat":"52.6910908","long":"-2.7598724","footfall":0},{"lat":"40.7739198","long":"-73.7409973","footfall":876},{"lat":"53.347424","long":"-6.2566586","footfall":1971},{"lat":"42.9828181","long":"-81.2158","footfall":493},{"lat":"34.1583476","long":"-99.2845272","footfall":0},{"lat":"42.2449866","long":"-71.7117526","footfall":0},{"lat":"40.7116446","long":"-74.0059076","footfall":350},{"lat":"53.3377951","long":"-6.2404114","footfall":115},{"lat":"40.7137888","long":"-73.9852147","footfall":4832},{"lat":"51.6925707","long":"-1.6926614","footfall":6179},{"lat":"54.958182449999995","long":"-1.7330810866717254","footfall":794},{"lat":"53.356529","long":"-6.2442572","footfall":0},{"lat":"53.3475841","long":"-6.2401912","footfall":1418},{"lat":"52.3343202","long":"-6.456993635930209","footfall":693},{"lat":"-37.5573128","long":"143.8691315","footfall":935},{"lat":"53.2647096","long":"-6.2291335","footfall":156},{"lat":"53.3314356","long":"-6.2646044","footfall":1087},{"lat":"-31.97977","long":"141.4741714","footfall":3233},{"lat":"51.5137208","long":"-0.1118637","footfall":1888}]'


        expected_result = '[{"lat":"53.3446","long":"-6.2668","footfall":100},{"lat":"53.3457","long":"-6.2678","footfall":150}]'
        self.assertEqual(result, expected_result)'''
