from utilities.CommonUtil import CommonUtil
import constants.config as config
import pandas as pd
import time

common_util = CommonUtil()


class FootfallHelper():
    @staticmethod
    def get_footfall_data():
        final_footfall_df = pd.DataFrame(columns=["lat", "long", "footfall"])
        footfall_df = common_util.datapull_db_model(config.FOOTFALL_ENDPOINT_STRING)
        latest_date = str(footfall_df['time'].values[0])

        for i in footfall_df.columns:
            if i != 'time':
                footfall_df = footfall_df.rename(columns={i: common_util.get_lat_long(i)})

        date_time = (latest_date[0:10] + " " + str(time.strftime("%H:00:00", time.localtime())))
        footfall_df = footfall_df.loc[footfall_df['time'] == date_time]
        footfall_df = footfall_df.drop(columns=['inrt_ts', 'time'])
        footfall_df_keys = list(footfall_df.keys())
        footfall_df_values = footfall_df.values
        for i, j in zip(footfall_df_keys, footfall_df_values[0]):
            try:
                lat, long = i.split(',')
            except:
                continue
            new_row = {'lat': lat, 'long': long, 'footfall': j}
            final_footfall_df = final_footfall_df.append(new_row, ignore_index=True)
        return final_footfall_df.to_json(orient='records')
