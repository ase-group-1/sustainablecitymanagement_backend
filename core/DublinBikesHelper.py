import traceback

from utilities.CommonUtil import CommonUtil
from utilities.ApiHandler import ApiHandler
import json
import pandas as pd
import numpy as np
from pandas.io.json import json_normalize
import constants.config as config
from datetime import datetime
import pickle
from sklearn import preprocessing

common_util = CommonUtil()
import warnings

warnings.filterwarnings("ignore")


class DublinBikesHelper():

    @staticmethod
    def get_bike_data():
        try:
            get_bike_data = common_util.datapull_db_model(config.BIKES_ENDPOINT_STRING)
            get_bike_data = pd.read_json(get_bike_data)
            return get_bike_data
        except:
            traceback.print_exc()
            return pd.DataFrame()

    @staticmethod
    def get_weather_data(df):
        weather_data = pd.DataFrame(columns=["day", "month", "year", "hour", "minute", "lat", "long", "temp"])
        for idx, row in df.iterrows():
            url_str = config.REAL_TIME_WEATHER_URL % (str(row["lat"]),str(row["long"]))
            data_dict=ApiHandler.api_get_from_xml_dict(url_str)
            json_data = json.dumps(data_dict)
            json_dict = json.loads(json_data)
            weather_test = json_normalize(json_dict)
            weather_df = weather_test.explode('weatherdata.meta.model')
            weather_df_new = weather_df.explode('weatherdata.product.time')
            _LEN = len(dict(weather_test)['weatherdata.product.time'][0])

            # Creating a new dataframe to store the weather data
            new_test = pd.DataFrame()
            new_test['day'] = [np.nan]
            new_test['month'] = [np.nan]
            new_test['year'] = [np.nan]
            new_test['hour'] = [np.nan]
            new_test['minute'] = [np.nan]
            new_test['lat'] = [np.nan]
            new_test['long'] = [np.nan]
            new_test['temp'] = [np.nan]

            date_time_obj = datetime.strptime(dict(weather_test)['weatherdata.product.time'][0][0]['@from'],
                                              "%Y-%m-%dT%H:%M:%SZ")
            new_test['day'][0] = date_time_obj.day
            new_test['month'][0] = date_time_obj.month
            new_test['year'][0] = date_time_obj.year
            new_test['hour'][0] = date_time_obj.hour
            new_test['minute'][0] = date_time_obj.minute
            new_test['lat'][0] = row["lat"]
            new_test['long'][0] = row["long"]
            if 'temperature' in dict(weather_test)['weatherdata.product.time'][0][0]['location'].keys():
                new_test['temp'][0] = dict(weather_test)['weatherdata.product.time'][0][0]['location']['temperature'][
                    '@value']
            new_test = new_test.dropna()
            new_test = new_test.reset_index(drop=True)
            weather_data = pd.concat([weather_data, new_test], axis=0)
        weather_data = weather_data[["temp", "lat", "long"]]
        return weather_data


    @staticmethod
    def transform_data_frame(df_2):
        df = df_2[["hrvst_ts", "lat", "longi", "avl_bks","stn_id"]]
        df["hrvst_ts"] = pd.to_datetime(df["hrvst_ts"])
        df.index = df["hrvst_ts"]
        df["year"] = df.index.year
        df["month"] = df.index.month
        df["day"] = df.index.day
        df["hour"] = df.index.hour
        df["minute"] = df.index.minute
        df["lat"] = df["lat"].astype(float)
        df["longi"] = df["longi"].astype(float)
        df.drop(["hrvst_ts"], axis=1, inplace=True)
        # df.rename(columns={"longi":"long"},inplace=True)
        df.reset_index(drop=True, inplace=True)
        return df
    @staticmethod
    def get_Dublin_bike_prediction():
        dublin_bike_df=DublinBikesHelper.get_bike_data()
        if not dublin_bike_df.empty:
            trans_df = DublinBikesHelper.transform_data_frame(dublin_bike_df)
            trans_df.rename(columns={"longi": "long"}, inplace=True)
            if trans_df.empty:
                return pd.DataFrame()
            weather_df = DublinBikesHelper.get_weather_data(trans_df)
            df_combined = pd.merge(trans_df, weather_df, on=['lat', 'long'])
            # ['year','month','day','hour','minute','temp','LATITUDE','LONGITUDE','AVAILABLE BIKES']
            df_combined_2 = df_combined[["year", "month", "day", "hour", "minute", "temp", "lat", "long", "avl_bks"]]
            classifier = pickle.load(open(config.ML_ENGINE_PICKLE_PATH, 'rb'))
            df_combined['predicted_value']=classifier.predict(df_combined_2)
            return df_combined







if __name__ == "__main__":
    df=DublinBikesHelper.get_Dublin_bike_prediction()
