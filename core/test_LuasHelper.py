import unittest
from unittest.mock import MagicMock
from core.LuasHelper import LuasHelper
import luas.api as luas
class TestLuasHelper(unittest.TestCase):

    def setUp(self):
        self.luas_helper = LuasHelper()

    def test_get_scheduled_luas_data_with_trams(self):
        mock_stop_details = {"trams": [{"destination": "Tallaght", "dueMinutes": "2"},
                                        {"destination": "The Point", "dueMinutes": "5"}]}
        mock_luas_client = MagicMock()
        mock_luas_client.stop_details.return_value = mock_stop_details
        luas.LuasClient = MagicMock(return_value=mock_luas_client)

        result = self.luas_helper.get_scheduled_luas_data("SAN")
        expected_result = mock_stop_details["trams"]

        self.assertEqual(result, expected_result)

    def test_get_scheduled_luas_data_without_trams(self):
        mock_stop_details = {}
        mock_luas_client = MagicMock()
        mock_luas_client.stop_details.return_value = mock_stop_details
        luas.LuasClient = MagicMock(return_value=mock_luas_client)

        result = self.luas_helper.get_scheduled_luas_data("SAN")
        expected_result = {}

        self.assertEqual(result, expected_result)


    def test_real_time_luas_station_data_with_empty_response(self):
        self.luas_helper.get_scheduled_luas_data = MagicMock(return_value={})

        result = self.luas_helper.real_time_luas_station_data("SAN")
        expected_result = "{}"

        self.assertEqual(result, expected_result)
