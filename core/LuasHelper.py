import json
import luas.api

class LuasHelper ():
    @staticmethod
    def get_scheduled_luas_data(station):
        luas_client = luas.api.LuasClient()
        stop_details = luas_client.stop_details(station)
        data = json.loads(json.dumps(stop_details))
        if 'trams' in data:
            return data["trams"]
        else :
            return {}

    @staticmethod
    def real_time_luas_station_data(station):
        if not station:
            response = "Please provide a station name at the end of URL (E.g ?station=SAN)"
        else:
            response = str(LuasHelper.get_scheduled_luas_data(station))
            response = response.replace("'",'"')
            print (response)
            if not response:
                response = "No Luas Scheduled at the moment"
        return str(response)