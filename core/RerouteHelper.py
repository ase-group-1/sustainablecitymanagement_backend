import os

from utilities.CommonUtil import CommonUtil
import pandas as pd
import shapely.wkt
from shapely.geometry import Point
common_util = CommonUtil()


class RerouteHelper():

    @staticmethod
    def read_rerouting_pickle(device_id):
        obj = pd.read_pickle(
            r'./rerouting/rerouting_json.pkl')
        df = pd.DataFrame([obj])
        lst = df[device_id]
        j = lst.to_json(orient='records')
        return j

