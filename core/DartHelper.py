
import json
import constants.config as config
from utilities.ApiHandler import ApiHandler

class DartHelper():
    @staticmethod
    def dart_realtime_location():
        url = config.DART_REAL_TIME_URL
        data = ApiHandler.api_get_from_xml_dict(url)
        if 'ArrayOfObjTrainPositions' in data.keys() and 'objTrainPositions' in data['ArrayOfObjTrainPositions'].keys():
            result_data = json.dumps(data['ArrayOfObjTrainPositions']['objTrainPositions'])
            return result_data
        else:
            return {}

