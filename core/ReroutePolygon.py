import os

import pandas as pd


class ReroutePolygon():

    @staticmethod
    def get_polygons():
        obj = pd.read_pickle(
            r'./rerouting/device_id_polygon_mapping.pkl')
        df = pd.DataFrame([obj])
        return df.to_json(default_handler=str, orient='records')
