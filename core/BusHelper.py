#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 10 12:58:42 2023

@author: varunsharma
"""
import sys
# sys.path.append("..")

from utilities.CommonUtil import CommonUtil as cu
from utilities.ApiHandler import ApiHandler

import pandas as pd
import geopandas as gpd
from datetime import datetime as dt
import geopandas as gpd
from datetime import datetime
from datetime import timedelta
import numpy as np
# from shapely.geometry import Point, LineString, shape
from shapely import wkt
import datetime
from constants import config
# import json

class BusHelper:
    def bus_route_dropdown_options_json(self):        
        trips_df = cu.get_db_data(
            f"SELECT * FROM {config.DB_DATABASE}.{config.BUS_TRIP_TABLE}", config.DB_DATABASE)
        routes_df = cu.get_db_data(
            f"SELECT * FROM {config.DB_DATABASE}.{config.BUS_ROUTES_TABLE}", config.DB_DATABASE)
        agency_df = cu.get_db_data(
            f"SELECT * FROM {config.DB_DATABASE}.{config.BUS_AGENCY_TABLE}", config.DB_DATABASE)
        calendar_dates_df = cu.get_db_data(
            f"SELECT * FROM {config.DB_DATABASE}.{config.BUS_CALENDAR_DATES_TABLE}", config.DB_DATABASE)
        calendar_df = cu.get_db_data(
            f"SELECT * FROM {config.DB_DATABASE}.{config.BUS_CALENDAR_TABLE}", config.DB_DATABASE)

        dropdown_options_df = pd.DataFrame(
            columns=['agency_name', 'route_name', 'route_headsign'])

        today = np.datetime64(datetime.datetime.today())

        weekday_matchup = {
            0: 'mon',
            1: 'tue',
            2: 'wed',
            3: 'thur',
            4: 'fri',
            5: 'sat',
            6: 'sun'
        }

        service_ids_active = set()

        for service_id in calendar_df.service_id.unique():
            st_dt = calendar_df[calendar_df.service_id ==
                                service_id].st_dt.values[0]
            end_dt = calendar_df[calendar_df.service_id ==
                                 service_id].end_dt.values[0]
            day_active_flag = calendar_df[calendar_df.service_id ==
                                          service_id][weekday_matchup[today.astype(datetime.datetime).isoweekday()-1]].values[0]
            if ((today > st_dt) and (today < end_dt) and (day_active_flag == 1)):
                service_ids_active.add(service_id)

        for service_id in calendar_dates_df.service_id.unique():
            if calendar_dates_df[calendar_dates_df.service_id == service_id].ex_date.values[0] == today:
                if calendar_dates_df[calendar_dates_df.service_id == service_id].ex_type.values[0] == '2':
                    service_ids_active.remove(service_id)
                if calendar_dates_df[calendar_dates_df.service_id == service_id].ex_type.values[0] == '1':
                    service_ids_active.add(service_id)

        routes_active = trips_df[trips_df.service_id.isin(
            service_ids_active)].route_id.unique()

        dropdown_options_df = routes_df[routes_df.route_id.isin(
            routes_active)][['route_id', 'agncy_id', 'route_s_nm', 'route_l_nm']]
        agency_dict = agency_df.set_index('agncy_id').to_dict()['agncy_nm']
        dropdown_options_df['agency_name'] = dropdown_options_df['agncy_id'].map(
            agency_dict)

        for agency_id in agency_df.agncy_id.unique():
            agency_routes = routes_df[routes_df.agncy_id == agency_id][[
                'route_id', 'agncy_id', 'route_s_nm', 'route_l_nm']]

        return dropdown_options_df.to_json(orient='records')

    # send json to react app
    def send_trip_id_json(self, agency_id, route_id, d_id):
        agency_id = agency_id
        direction_id = d_id
        route_id = route_id

        trips_df = cu.get_db_data(
            f"SELECT * FROM {config.DB_DATABASE}.{config.BUS_TRIP_TABLE}", config.DB_DATABASE)
        calendar_dates_df = cu.get_db_data(
            f"SELECT * FROM {config.DB_DATABASE}.{config.BUS_CALENDAR_DATES_TABLE}", config.DB_DATABASE)
        calendar_df = cu.get_db_data(
            f"SELECT * FROM {config.DB_DATABASE}.{config.BUS_CALENDAR_TABLE}", config.DB_DATABASE)

        today = np.datetime64(datetime.datetime.today())

        weekday_matchup = {
            0: 'mon',
            1: 'tue',
            2: 'wed',
            3: 'thur',
            4: 'fri',
            5: 'sat',
            6: 'sun'
        }

        service_ids_active = set()

        for service_id in calendar_df.service_id.unique():
            st_dt = calendar_df[calendar_df.service_id ==
                                service_id].st_dt.values[0]
            end_dt = calendar_df[calendar_df.service_id ==
                                 service_id].end_dt.values[0]
            day_active_flag = calendar_df[calendar_df.service_id ==
                                          service_id][weekday_matchup[today.astype(datetime.datetime).isoweekday()-1]].values[0]
            if ((today > st_dt) and (today < end_dt) and (day_active_flag == 1)):
                service_ids_active.add(service_id)

        for service_id in calendar_dates_df.service_id.unique():
            if calendar_dates_df[calendar_dates_df.service_id == service_id].ex_date.values[0] == today:
                if calendar_dates_df[calendar_dates_df.service_id == service_id].ex_type.values[0] == '2':
                    service_ids_active.remove(service_id)
                if calendar_dates_df[calendar_dates_df.service_id == service_id].ex_type.values[0] == '1':
                    service_ids_active.add(service_id)
        selected_trip_ids = trips_df[(trips_df.route_id == route_id) & (trips_df.service_id.isin(
            service_ids_active)) & (trips_df.d_id == direction_id)].trip_id.unique()
        if len(selected_trip_ids) == 0:
            # print('No trip found')
            return {"trip_id": []}
        active_trip_id = []
        header={'x-api-key': f'{config.BUS_REAL_TIME_API_KEY}'}
        gtfsR_json = ApiHandler.api_get_api_content_json(url=config.BUS_REAL_TIME_URL, header=header)
        if gtfsR_json == {}:
            return {'Realtime API Error': 'No data returned'}
        
        for t in gtfsR_json['entity']:
            if t['trip_update']['trip']['schedule_relationship'] != 'SCHEDULED':
                continue
            active_trip_id.append(t['trip_update']['trip']['trip_id'])

        selected_trip_id = list(set(selected_trip_ids) & set(active_trip_id))
        trip_id_json = {
            "trip_id": selected_trip_id
        }

        return trip_id_json

    def send_shapes_stops_realtime_json(self, selected_trip_id):

        trips_df = cu.get_db_data(
            f"SELECT * FROM {config.DB_DATABASE}.{config.BUS_TRIP_TABLE} WHERE trip_id = \'{selected_trip_id}\'", config.DB_DATABASE)
        trips_df.drop(columns=['inrt_ts'], inplace=True)

        stop_times_df = cu.get_db_data(
            f"SELECT * FROM {config.DB_DATABASE}.{config.BUS_STOP_TIMES_TABLE} WHERE trip_id = \'{selected_trip_id}\'", config.DB_DATABASE)
        stop_times_df.drop(columns=['inrt_ts'], inplace=True)

        shapes_gdf = cu.get_db_data(
            f"SELECT * FROM {config.DB_DATABASE}.{config.BUS_SHAPES_TABLE} WHERE shape_id = \'{trips_df.shape_id.values[0]}\'", config.DB_DATABASE)
        shapes_gdf.drop(columns=['inrt_ts'], inplace=True)

        stops_gdf = cu.get_db_data(
            f"SELECT * FROM {config.DB_DATABASE}.{config.BUS_STOPS_TABLE} WHERE stop_id IN {tuple(stop_times_df.stop_id.unique())}", config.DB_DATABASE)
        stops_gdf.drop(columns=['inrt_ts'], inplace=True)

        shapes_gdf.geometry = shapes_gdf.geometry.apply(wkt.loads)
        # stops_gdf.geometry = stops_gdf.geometry.apply(wkt.loads)

        shapes_gdf = gpd.GeoDataFrame(
            shapes_gdf, geometry='geometry', crs="EPSG:4326")
        # stops_gdf = gpd.GeoDataFrame(
        #     stops_gdf, geometry='geometry', crs="EPSG:4326")
        stop_times_df.merge(stops_gdf, on='stop_id', how='left')

        # merge stop times with stops
        stop_times_df = stop_times_df.merge(
            stops_gdf, on='stop_id', how='left')
        stop_times_df.geometry = stop_times_df.geometry.apply(wkt.loads)
        stop_times_df = gpd.GeoDataFrame(
            stop_times_df, geometry='geometry', crs="EPSG:4326")

        gtfsR_json = ApiHandler.api_get_api_content_json(url=config.BUS_REAL_TIME_URL, header={'X-API-Key': config.BUS_REAL_TIME_API_KEY})
        # gtfsR_json = gtfsR_json
        selected_trip_update = {}
        if gtfsR_json == {}:
            return {"error": "No Realtime Data"}
        for t in gtfsR_json['entity']:
            if t['trip_update']['trip']['schedule_relationship'] != 'SCHEDULED':
                continue
            if t['trip_update']['trip']['trip_id'] == selected_trip_id:
                selected_trip_update = t['trip_update']
                break
        # stop_times_df['realtime_delay'] = pd.to_timedelta(stop_times_df.realtime_delay, unit='m')

        # create a new column in stop_times_df to store the realtime stop times
        stop_times_df['realtime_delay'] = 0
        for st in selected_trip_update['stop_time_update']:
            if 'stop_sequence' in st:    
                if 'arrival' in st:
                    if 'delay' in st['arrival']:
                        # assign to realtime_delay column as time delta)
                        if st['arrival']['delay'] < 0:
                            stop_times_df.loc[stop_times_df.stop_sq == str(st['stop_sequence']), 'realtime_delay'] = timedelta(seconds=float(-1*st['arrival']['delay']))
                            # print(f'negative delay in arrival {st["arrival"]["delay"]}')
                            stop_times_df.loc[stop_times_df.stop_sq == str(st['stop_sequence']), 'arr_tm'] -= timedelta(seconds=float(st['arrival']['delay']))
                        
                        else:
                            stop_times_df.loc[stop_times_df.stop_sq == str(st['stop_sequence']), 'realtime_delay'] = timedelta(seconds=float(st['arrival']['delay']))
                            stop_times_df.loc[stop_times_df.stop_sq == str(st['stop_sequence']), 'arr_tm'] += timedelta(seconds=float(st['arrival']['delay']))
                elif 'departure' in st:
                    if 'delay' in st['departure']:
                        # assign to realtime_delay column as time delta
                        if st['departure']['delay'] < 0:
                            stop_times_df.loc[stop_times_df.stop_sq == str(st['stop_sequence']), 'realtime_delay'] = timedelta(seconds=float(-1*st['departure']['delay']))
                            stop_times_df.loc[stop_times_df.stop_sq == str(st['stop_sequence']), 'dep_tm'] -= timedelta(seconds=float(st['departure']['delay']))
                        
                        else:
                            stop_times_df.loc[stop_times_df.stop_sq == str(st['stop_sequence']), 'realtime_delay'] = timedelta(seconds=float(st['departure']['delay']))
                            stop_times_df.loc[stop_times_df.stop_sq == str(st['stop_sequence']), 'dep_tm'] += timedelta(seconds=float(st['departure']['delay']))

        stop_times_df['arr_tm'] = stop_times_df['arr_tm'].apply(lambda x: x.time())
        stop_times_df['dep_tm'] = stop_times_df['dep_tm'].apply(lambda x: x.time())

        # add realtime delay to the stop times
        # stop_times_df['arr_tm'] = stop_times_df['arr_tm'] + stop_times_df['realtime_delay']
        # stop_times_df['dep_tm'] = stop_times_df['dep_tm'] + stop_times_df['realtime_delay']
        # convert all the columns to string
        stop_times_df[['arr_tm', 'dep_tm', 'realtime_delay']] = stop_times_df[['arr_tm', 'dep_tm', 'realtime_delay']].astype(str)
        # print(stop_times_df.head(1))

        shapes_gdf_json = shapes_gdf.to_json()
        stop_times_df_json = stop_times_df.to_json()

        response_json = {
            "trip_id": selected_trip_id,
            "shape_id": shapes_gdf.shape_id.values[0],
            "shape": shapes_gdf_json,
            "stops_info": stop_times_df_json,
        }

        # return response_json, stop_times_df
        return response_json

    # def test_all(self):
    #     # test get all trips
    #     dropdown_options = self.bus_route_dropdown_options_json()
    #     # {'route_id': '2954_46068', 
    #     #  'agncy_id': '7778019',
    #     #  'route_s_nm': '151', 
    #     #  'route_l_nm': 'Foxborough (Balgaddy Rd) - Docklands (East Rd)',
    #     #  'agency_name': 'Dublin Bus \\/ Bus Átha Cliath'}
    #     selected_trip_id, gtfsR_json = self.send_trip_id_json(agency_id='7778019', route_id='2954_46068', d_id='1')
    #     print(selected_trip_id)
    #     response_json, stop_times_df= self.send_shapes_stops_realtime_json(
    #         selected_trip_id='2970_4389', gtfsR_json=gtfsR_json)
    #     # return selected_trip_id, gtfsR_json
    #     return response_json, stop_times_df


# if __name__ == "__main__":
#     # response_json, stop_times_df, gtfsR_json = BusHelper().send_shapes_stops_realtime_json('2970_4448')
#     response_json, stop_times_df = BusHelper().test_all()
#     # dropdown_options = eval(dropdown_options)
#     # print(type(dropdown_options))
#     # for option in dropdown_options:
#     #     if option['route_s_nm'] == '151':
#     #         selected_dopdown_option = option
#     #         print(selected_dopdown_option)
#     #         break

