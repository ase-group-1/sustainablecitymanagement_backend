import hashlib
import uuid
from dbhandler.UserModel import UserModel
from ast import literal_eval
from utilities.CommonUtil import CommonUtil
import constants.config as config
import pandas as pd
import traceback

common_util = CommonUtil()
class LoggedInUserHelper():
    @staticmethod
    def get_all_user():
        # Getting all the data from Databased
        user_list = literal_eval(common_util.datapull_db_model(config.USER_DATA_MODEL))
        user_df = pd.DataFrame(user_list)
        if not user_df.empty:
            user_df = user_df[['public_id', 'name', 'email', 'role_name']]
        return user_df

    @staticmethod
    def verify_user_password(email, password):

        session=common_util.get_session_based_on_model(config.USER_DATA_MODEL)
        query = session.query(common_util.get_model_object(config.USER_DATA_MODEL)).filter_by(email=email).first()
        if query == None:
            return False, None
        if query.password == str(hashlib.md5(password.encode()).hexdigest()):
            return True,query

    @staticmethod
    def user_sign_up(email, password, name, role_name):
        if role_name.lower() == 'admin':
            role_id = 2
        elif role_name.lower() == 'manager':
            role_id = 1
        else:
            role_id = 0
        status, user = LoggedInUserHelper.verify_user_password(email=email, password=password)
        if not user:
            try:
                public_id = str(uuid.uuid4())
                passwrd = str(hashlib.md5(password.encode()).hexdigest())
                session = common_util.get_session_based_on_model(config.USER_DATA_MODEL)
                user = UserModel(
                    public_id=public_id,
                    name=name,
                    email=email,
                    password=passwrd,
                    role_name=role_name,
                    role_id=role_id
                )
                session.add(user)
                session.commit()
                return True
            except:
                traceback.print_exc()
                return False
        else:
            return False
