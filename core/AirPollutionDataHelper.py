import json
import traceback

import numpy as np
import pandas as pd
from datetime import datetime, timedelta
import pickle

import constants.config as config
from utilities.CommonUtil import CommonUtil

from utilities.ApiHandler import ApiHandler


class AirPollutionDataHelper():
    common_util = CommonUtil()

    @staticmethod
    def check_sensor_is_valid(sensorname):
        try:
            query = "Select sr_nm as serialNo from " + config.DB_DATABASE + "." + config.DUBLIN_AIR_SENSOR_LIST_TABLE + " where sr_nm = '" + str(
                sensorname) + "';"
            result_dataFrame = CommonUtil.get_db_data(query, config.DB_DATABASE)
            if (result_dataFrame.empty):
                return False
            else:
                return True
        except:
            traceback.print_exc()
            return False

    @staticmethod
    def get_pm2_5_data(sensorname, timeOfIntent):
        d1 = str(timeOfIntent + timedelta(minutes=15))
        d2 = str(timeOfIntent - timedelta(minutes=15))
        d3 = timeOfIntent - timedelta(days=365) - timedelta(minutes=15)
        d4 = timeOfIntent - timedelta(days=365) + timedelta(minutes=15)
        sensor_lower_name = str(sensorname).strip().lower().replace("-", "_")
        stage_table_name = config.DUBLIN_AIR_SENSOR_DUBLIN_STAGE_TABLE + sensor_lower_name
        query = "select datetime,pm2_5 from " + stage_table_name + " where datetime between '" + d2 + "' and '" + d1 + "' ORDER BY datetime DESC;"
        print(query)
        dbresult = CommonUtil.get_db_data(query, config.DB_STAGE_DATABASE)
        if (not dbresult.empty):
            intent_db_result = dbresult[dbresult['datetime'] == str(timeOfIntent)]
            if not intent_db_result.empty:
                return intent_db_result.iloc[0]['pm2_5']
            else:
                intent_db_result = intent_db_result[
                    (intent_db_result['datetime'] > str(d3)) & (intent_db_result['datetime'] < str(d4))].sort_values(
                    by='datetime')
                if not intent_db_result.empty:
                    return intent_db_result.iloc[0]['pm2_5']
                else:
                    return -1
        else:
            # Search in CSV File
            pathfile= config.CSV_PATH
            file_name = sensorname + ".csv"
            try:
                csv_df = pd.read_csv(pathfile + file_name)
                csv_df_intent = csv_df[csv_df['datetime'] == str(timeOfIntent)]
                if (csv_df_intent.empty):
                    csv_df_intent = csv_df[(csv_df['datetime'] > str(d3)) & (csv_df['datetime'] < str(d4))].sort_values(
                        by='datetime')
                    if (not csv_df_intent.empty):
                        return (csv_df_intent.iloc[0]['pm2_5'])
                    else:
                        return -1
                else:
                    return (csv_df_intent.iloc[0]['pm2_5'])

            except:
                print("File Not Found")
                return -1

    @staticmethod
    def get_sensor_data_from_data_base(senorName):
        try:
            if AirPollutionDataHelper.check_sensor_is_valid(senorName):
                sensor_lower_name = str(senorName).strip().lower().replace("-", "_")
                prod_table_name = config.DUBLIN_AIR_SENSOR_TABLE_TAG + sensor_lower_name
                query = "select * from " + config.DB_DATABASE + "." + prod_table_name + " ORDER BY datetime DESC limit 1;"
                sensor_dataframe = CommonUtil.get_db_data(query, config.DB_DATABASE)
                if not sensor_dataframe.empty:
                    return sensor_dataframe[['datetime', 'pm2_5']].to_json(orient="records")
                else:
                    return {}
        except:
            traceback.print_exc()
            return {}

    @staticmethod
    def predict_sensor_value(senorname, metric, data):
        try:
            # Get the data from the POST request in the form of json array
            # device_data  = {"":"x"}

            search = "gbr" + "_" + senorname + "_" + metric + ".pkl"
            filename_model = config.ML_AIR_POLLUTION_PICKLE_PATH + search
            # Load the model using pickle
            saved_model = pickle.load(open(filename_model, 'rb'))
            # Make prediction using model loaded
            prediction = saved_model.predict([data])
            return prediction[0]
        except:
            traceback.print_exc()
            return -1

    @staticmethod
    def get_sensor_predicted_sata(senorName, year, month, day, hour, minute):
        try:
            if (AirPollutionDataHelper.check_sensor_is_valid(senorName)):
                last_year_date = datetime(year=year, month=month, day=day, hour=hour, minute=minute, second=0)
                print(last_year_date)
                pm2_5 = AirPollutionDataHelper.get_pm2_5_data(senorName, last_year_date)
                if pm2_5==-1:
                    return -1
                else :
                    sensor_lower_name = str(senorName).strip().replace("-", "_")
                    predict_data=list([year,month,day,hour,minute,pm2_5])
                    predict_data=[predict_data]
                    predict_value=AirPollutionDataHelper.predict_sensor_value(sensor_lower_name, "pm2_5", predict_data)
                    predict_value=round(predict_value,2)
                    result={"Sensor Name": senorName,"Predicted Pm2_5 Val":predict_value}
                    return result
        except:
            traceback.print_exc()
            return -1

    @staticmethod
    def get_all_sensor_data():
        try:
            query = "Select * from " + config.DB_DATABASE + "." + config.DUBLIN_AIR_SENSOR_LIST_TABLE + ";"
            result_dataFrame = CommonUtil.get_db_data(query, config.DB_DATABASE)
            if(not result_dataFrame.empty):
                result_dataFrame.drop(columns=['cycl_time_id','inrt_ts'],inplace=True)
            return result_dataFrame
        except:
            traceback.print_exc()
            return pd.DataFrame()


