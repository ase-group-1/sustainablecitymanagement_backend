
from dbhandler.DbConnection import DbConnection
from constants import config
from sqlalchemy import Column, String
import pandas as pd

dbconnection = DbConnection(config.DB_CONNECT_URL)
class LuasStationListModel(dbconnection.Base):


    __tablename__ = config.DUBLIN_LUAS_STATION_LIST_TABLE

    abrev = Column(String, primary_key=True)
    isParkRide = Column(String)
    isCycleRide = Column(String)
    lat = Column(String)
    longi = Column(String)
    pronunciation = Column(String)
    stpnm = Column(String)
    lineType = Column(String)

