from dbhandler.DbConnection import DbConnection
from sqlalchemy import Column, String
from constants import config

dbconnection = DbConnection(config.DB_CONNECT_URL)

class BusStopTimesModel(dbconnection.Base):
    """
    Stop Times Table
    idx
    trip_id
    arr_tm
    dep_tm
    stop_id
    stop sa
    inrt_ts
    """
    __tablename__ = f'{config.BUS_STOP_TIMES_TABLE}'
    idx = Column(String, primary_key=True)
    trip_id = Column(String)
    arr_tm = Column(String)
    dep_tm = Column(String)
    stop_id = Column(String)
    stop_seq = Column(String)
    inrt_ts = Column(String)