
from constants import config
from sqlalchemy import Column, String
import pandas as pd
from dbhandler.DbConnection import DbConnection
db= DbConnection(config.DB_CONNECT_URL)


class TrafficDensityModel(db.Base):
    __tablename__ = config.DUBLIN_TRAFFIC_DENSITY_TABLE

    id = Column(String, primary_key=True)
    url = Column(String)
    lat = Column(String)
    lon = Column(String)


