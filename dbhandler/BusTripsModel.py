from dbhandler.DbConnection import DbConnection
from sqlalchemy import Column, String
from constants import config

dbconnection = DbConnection(config.DB_CONNECT_URL)

class BusTripsModel(dbconnection.Base):
    """
    Trips Table
    trip id
    route id
    trip_hds
    trip_s_nm
    did
    block_id
    shape id
    service_id
    inrt_ts
    """
    __tablename__ = f'{config.BUS_TRIP_TABLE}'
    trip_id = Column(String, primary_key=True)
    route_id = Column(String)
    trip_hds = Column(String)
    trip_s_nm = Column(String)
    did = Column(String)
    block_id = Column(String)
    shape_id = Column(String)
    service_id = Column(String)
    inrt_ts = Column(String)