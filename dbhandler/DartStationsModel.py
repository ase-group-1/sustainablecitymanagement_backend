from constants import config
from sqlalchemy import Column, String
import pandas as pd
from dbhandler.DbConnection import DbConnection

dbconnection = DbConnection(config.DB_CONNECT_URL)


class DartStationsModel(dbconnection.Base):
    __tablename__ = config.DUBLIN_DART_STATION_TABLE

    stn_desc = Column(String, primary_key=True)
    lat = Column(String)
    longi = Column(String)
    stn_cd = Column(String)
    stn_id = Column(String)



