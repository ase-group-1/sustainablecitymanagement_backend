import traceback

from constants import config
from sqlalchemy import Column, String,Integer
import pandas as pd
from dbhandler.DbConnection import DbConnection
db= DbConnection(config.DB_CONNECT_URL_USER_DATABASE)


class UserModel(db.Base):
    __tablename__ = config.USER_DETAILS

    id = Column(Integer, primary_key=True)
    public_id = Column(String(50), unique=True)
    name = Column(String(100))
    email = Column(String(70), unique=True)
    password = Column(String(80))
    role_name = Column(String(20))
    role_id = Column(Integer)

