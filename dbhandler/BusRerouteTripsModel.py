from constants import config
from sqlalchemy import Column, String
from dbhandler.DbConnection import DbConnection
import pandas as pd

dbconnection = DbConnection(config.DB_CONNECT_URL)


class BusRerouteTripsModel(dbconnection.Base):
    __tablename__ = config.DUBLIN_REROUTE_BUS_TRIPS

    route_id = Column(String, primary_key=True)
    trip_id = Column(String)
    trip_hds = Column(String)
    trip_s_nm = Column(String)
    d_id = Column(String)
    block_id = Column(String)
    shape_id = Column(String)
    service_id = Column(String)
    inrt_ts = Column(String)



