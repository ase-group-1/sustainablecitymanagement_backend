from dbhandler.DbConnection import DbConnection
from sqlalchemy import Column, String
from constants import config

dbconnection = DbConnection(config.DB_CONNECT_URL)

class BusCalendarDatesModel(dbconnection.Base):
    """
    Calendar Dates Table
    idx
    service_id
    ex_date
    ex_type
    inrt_ts
    """
    __tablename__ = f'{config.BUS_CALENDAR_DATES_TABLE}'
    idx = Column(String, primary_key=True)
    service_id = Column(String)
    ex_date = Column(String)
    ex_type = Column(String)
    inrt_ts = Column(String)