from dbhandler.DbConnection import DbConnection
from sqlalchemy import Column, String
from constants import config

dbconnection = DbConnection(config.DB_CONNECT_URL)

class BusAgencyModel(dbconnection.Base):
    """
    Agency Table
    agncy_id
    agncy_name
    inrt_ts
    """
    __tablename__ = f'{config.BUS_AGENCY_TABLE}'
    agncy_id = Column(String, primary_key=True)
    agncy_name = Column(String)
    inrt_ts = Column(String)