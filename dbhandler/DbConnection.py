from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import constants.config as config

class DbConnection(object) :
    SQLALCHEMY_DATABASE_URL = ""
    SessionLocal=None
    Base = None
    dbFetchSession = None
    engine=None

    def __init__(self,data_base_url):
        self.SQLALCHEMY_DATABASE_URL = data_base_url
        self.engine = create_engine(
            self.SQLALCHEMY_DATABASE_URL
        )
        self.SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=self.engine)
        self.Base = declarative_base()
        self.dbFetchSession = sessionmaker(bind=self.engine)



    def get_db(self,):
            db = self.SessionLocal()
            try:
                yield db
            finally:
                db.close()

    def get_model_session(self,model_name):
        self.dbFetchSession().query().f











