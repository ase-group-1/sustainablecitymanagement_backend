import pandas as pd
from dbhandler.DbConnection import DbConnection
from sqlalchemy import Column, String
from constants import config

dbconnection = DbConnection(config.DB_CONNECT_URL)


class BikesModel(dbconnection.Base):
    __tablename__ = config.DUBLIN_BIKES_TABLE

    id = Column(String, primary_key=True)
    hrvst_ts = Column(String)
    stn_id = Column(String)
    avl_bks_std = Column(String)
    bks_std = Column(String)
    avl_bks = Column(String)
    bnk = Column(String)
    bns = Column(String)
    ls_updt_ts = Column(String)
    sta = Column(String)
    addr_ln = Column(String)
    stn_nm = Column(String)
    lat = Column(String)
    longi = Column(String)

