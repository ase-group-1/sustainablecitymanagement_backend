from constants import config
from sqlalchemy import Column, String
from dbhandler.DbConnection import DbConnection
import pandas as pd

dbconnection = DbConnection(config.DB_CONNECT_URL)


class BusRerouteShapeModel(dbconnection.Base):
    __tablename__ = config.DUBLIN_REROUTE_BUS_SHAPES

    shape_id = Column(String, primary_key=True)
    geometry = Column(String)
    inrt_ts = Column(String)


