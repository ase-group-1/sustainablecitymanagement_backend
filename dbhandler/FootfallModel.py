from constants import config
from sqlalchemy import Column, String
from dbhandler.DbConnection import DbConnection
import pandas as pd
db=DbConnection(config.DB_CONNECT_URL)


class FootfallModel(db.Base):
    __tablename__ = config.DUBLIN_FOOTFALL_TABLE

    time = Column(String, primary_key=True)
    Aston_Quay = Column(String)
    Bachelors_walk = Column(String)
    Baggot_st_lower = Column(String)
    Baggot_st_upper = Column(String)
    Capel_st = Column(String)
    College_Green_Bank_Of_Ireland = Column(String)
    College_Green_Church_Lane = Column(String)
    Westmoreland_st = Column(String)
    Burgh_Quay = Column(String)
    Dame_Street = Column(String)
    Dawson_Street = Column(String)
    Grafton_st = Column(String)
    Nassau_Street = Column(String)
    Clanwilliam_place = Column(String)
    Henry_Street = Column(String)
    Halfpenny_Bridge = Column(String)
    Mary_st = Column(String)
    Newcomen_Bridge = Column(String)
    North_Wall_Quay = Column(String)
    Parnell_St = Column(String)
    Princes_st_North = Column(String)
    Enniskerry_Road = Column(String)
    Richmond_st_south = Column(String)
    Talbot_st = Column(String)
    Fleet_street = Column(String)
    inrt_ts = Column(String)


