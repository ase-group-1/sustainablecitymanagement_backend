from dbhandler.DbConnection import DbConnection
from sqlalchemy import Column, String
from constants import config

dbconnection = DbConnection(config.DB_CONNECT_URL)

class BusCalendarModel(dbconnection.Base):
    """
    Calendar Table
    idx
    service_id
    mon
    tue
    wed
    thur
    fri
    sat
    sun
    st_dt
    end dt
    inrt_ts
    """
    __tablename__ = f'{config.BUS_CALENDAR_TABLE}'
    idx = Column(String, primary_key=True)
    service_id = Column(String)
    mon = Column(String)
    tue = Column(String)
    wed = Column(String)
    thur = Column(String)
    fri = Column(String)
    sat = Column(String)
    sun = Column(String)
    st_dt = Column(String)
    end_dt = Column(String)
    inrt_ts = Column(String)