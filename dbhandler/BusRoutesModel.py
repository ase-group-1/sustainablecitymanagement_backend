from dbhandler.DbConnection import DbConnection
from sqlalchemy import Column, String
from constants import config

dbconnection = DbConnection(config.DB_CONNECT_URL)

class BusRoutesModel(dbconnection.Base):
    """
    Routes Table
    route_id
    agncy_ id
    route s nm
    route_l_nm
    inrt_ts
    """
    __tablename__ = f'{config.BUS_ROUTES_TABLE}'
    route_id = Column(String, primary_key=True)
    agncy_id = Column(String)
    route_s_nm = Column(String)
    route_l_nm = Column(String)
    inrt_ts = Column(String)