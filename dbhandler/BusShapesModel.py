from dbhandler.DbConnection import DbConnection
from sqlalchemy import Column, String
from constants import config

dbconnection = DbConnection(config.DB_CONNECT_URL)

class BusShapesModel(dbconnection.Base):
    """
    Shapes Table
    shape_id
    geometry
    inrt ts
    """
    __tablename__ = f'{config.BUS_SHAPES_STRING}'
    shape_id = Column(String, primary_key=True)
    geometry = Column(String)
    inrt_ts = Column(String)