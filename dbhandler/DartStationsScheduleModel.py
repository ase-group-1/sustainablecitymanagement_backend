from constants import config
from sqlalchemy import Column, String
import pandas as pd
from dbhandler.DbConnection import DbConnection
db = DbConnection(config.DB_CONNECT_URL)


class DartStationsScheduleModel(db.Base):
    __tablename__ = config.DUBLIN_DART_STATION_SCHEDULE_TABLE

    stn_desc = Column(String, primary_key=True)
    srvr_ts = Column(String)
    trn_cd = Column(String)
    stn_nm = Column(String)
    stn_cd = Column(String)
    qry_ts = Column(String)
    trn_dt = Column(String)
    org_nm = Column(String)
    destn = Column(String)
    org_ts = Column(String)
    destn_ts = Column(String)
    sta = Column(String)
    lst_loc = Column(String)
    due_ts = Column(String)
    late = Column(String)
    exp_arr = Column(String)
    exp_dep = Column(String)
    sch_arr = Column(String)
    sch_dep = Column(String)
    drtn = Column(String)
    trn_typ = Column(String)
    loc_typ = Column(String)





