from dbhandler.DbConnection import DbConnection
from sqlalchemy import Column, String
from constants import config

dbconnection = DbConnection(config.DB_CONNECT_URL)

class BusStopsModel(dbconnection.Base):
    """
    Stops Table
    stop_id
    stop_cd
    stop_nm
    geometry
    inrt_ts
    """
    __tablename__ = f'{config.BUS_STOPS_TABLE}'
    stop_id = Column(String, primary_key=True)
    stop_cd = Column(String)
    stop_nm = Column(String)
    geometry = Column(String)
    inrt_ts = Column(String)