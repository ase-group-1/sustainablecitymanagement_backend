from flask_cors import CORS
from flask import Flask, request, jsonify, make_response
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, get_jwt_identity, unset_jwt_cookies

from utilities.CommonUtil import CommonUtil
from core.DartHelper import DartHelper
from core.LuasHelper import LuasHelper
from core.FootfallHelper import FootfallHelper
from core.LoggedInUserHelper import LoggedInUserHelper
from core.AirPollutionDataHelper import AirPollutionDataHelper
from core.RerouteHelper import RerouteHelper
from core.ReroutePolygon import ReroutePolygon
from core.DublinBikesHelper import DublinBikesHelper
from core.BusHelper import BusHelper
import constants.config as config
from datetime import timedelta
from constants.enums import Role_ids
import logging
import datetime
import os 
from logging.handlers import TimedRotatingFileHandler

app = Flask(__name__)
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = timedelta(minutes=30)
app.config["JWT_ALGORITHM"] = config.JWT_ALGORITHM
app.config["JWT_SECRET_KEY"] = config.JWT_SECRET_KEY

CORS(app)

log_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
log_folder = 'logs'
if not os.path.exists(log_folder):
    os.makedirs(log_folder)
log_file = os.path.join(log_folder, datetime.datetime.now().strftime('%Y-%m-%d') + '.log')

log_handler = TimedRotatingFileHandler(log_file, when='midnight', interval=1)
log_handler.setLevel(logging.ERROR)
log_handler.setFormatter(log_formatter)

app.logger.addHandler(log_handler)
jwt = JWTManager(app)
common_util = CommonUtil()


def requires_role(roles):
    def decorator(f):
        @jwt_required()
        def wrapper(*args, **kwargs):
            # retrieve user data from JWT token
            identity = get_jwt_identity()
            user_role = identity.get("role_id", None)
            # check if user has required role
            if user_role < roles:
                return {"message": "Unauthorized"}, 401
            return f(*args, **kwargs)

        return wrapper

    return decorator


@app.route('/')
def index_startpage():
    return "Welcome To Our Backend Flask Application"


@app.route(config.SLASH + config.BIKES_ENDPOINT_STRING)
def get_bikes_data():
    try:
        return common_util.datapull_db_model(config.BIKES_ENDPOINT_STRING)
    except Exception as e:
        app.logger.error(str(e))


@app.route(config.SLASH + config.DART_STATIONS_ENDPOINT_STRING)
def get_dart_stations():
    try:
        return common_util.datapull_db_model(config.DART_STATIONS_ENDPOINT_STRING)
    except Exception as e:
        app.logger.error(str(e))


@app.route(config.SLASH + config.DART_STATIONS_SCHEDULE_ENDPOINT_STRING)
def get_dartstationschedulelocation():
    try:
        return common_util.datapull_db_model(config.DART_STATIONS_SCHEDULE_ENDPOINT_STRING)
    except Exception as e:
        app.logger.error(str(e))


@app.route(config.SLASH + config.FOOTFALL_ENDPOINT_STRING)
def get_get_footfall_data():
    try:
        return FootfallHelper.get_footfall_data()
    except Exception as e:
        app.logger.error(str(e))


@app.route(config.SLASH + config.LUAS_STATIONS_ENDPOINT_STRING)
def get_get_luas_station_list_data():
    try:
        return common_util.datapull_db_model(config.LUAS_STATIONS_ENDPOINT_STRING)
    except Exception as e:
        app.logger.error(str(e))


@app.route(config.SLASH + config.TRAFFIC_DENSITY_ENDPOINT_STRING)
def get_traffic_density():
    try:
        return common_util.datapull_db_model(config.TRAFFIC_DENSITY_ENDPOINT_STRING)
    except Exception as e:
        app.logger.error(str(e))


@app.route(config.SLASH + config.PREDICTED_AVAILABLE_BIKES)
def get_predicted_available_bikes():
    try:
        res= DublinBikesHelper.get_Dublin_bike_prediction()
        return res.to_json(orient='records')
    except Exception as e:
        app.logger.error(str(e))


@app.route(config.SLASH + config.DART_REALTIME_LOCATION)
def get_dart_realtime_location():
    try:
        return str(DartHelper.dart_realtime_location())
    except Exception as e:
        app.logger.error(str(e))


@app.route(config.SLASH + config.LUAS_REAL_TIME_STATION_STATUS)
def get_real_time_luas_station_status():
    try:
        station = request.args.get(config.LUAS_STATION_URL_PARAM)
        return LuasHelper.real_time_luas_station_data(station)
    except Exception as e:
        app.logger.error(str(e))


@app.route(config.SLASH + config.GET_ALL_USER_ENDPOINT_STRING, methods=['GET'],
           endpoint=config.GET_ALL_USER_ENDPOINT_STRING)
def get_all_user():
    try:
        return LoggedInUserHelper.get_all_user().to_json(orient='records')
    except Exception as e:
        app.logger.error(str(e))


@app.route(config.SLASH + config.USER_LOGOUT_ENDPOINT_STRING, methods=['GET'],
           endpoint=config.USER_LOGOUT_ENDPOINT_STRING)
# to confirm if the user is authenticated
def log_out():
    try:
        response = jsonify({'message': "Successfully Logged Out"})
        return response
    except Exception as e:
        app.logger.error(str(e))



@app.route(config.SLASH + config.USER_LOGIN_ENDPOINT_STRING, methods=['POST'],
           endpoint=config.USER_LOGIN_ENDPOINT_STRING)
def login():
    # creates dictionary of form data
    try:
        auth = request.json
        if not auth or not auth.get('email') or not auth.get('password'):
            # returns 401 if any email or / and password is missing
            return make_response('Could not verify', 401, {'WWW-Authenticate': 'Basic realm ="Login required !!"'})
        status, user = LoggedInUserHelper.verify_user_password(email=auth.get('email'), password=auth.get('password'))
        if status and user is not None:
            access_token = create_access_token(identity={
                "user_id": user.id,
                "username": user.name,
                "role": user.role_name,
                "role_id": user.role_id
            })
            return jsonify(message="Login Succeeded!", access_token=access_token), 201
        else:
            return make_response('Could not verify', 401, {'WWW-Authenticate': 'Basic realm ="User does not exist !!"'})
        return make_response('Could not verify', 403, {'WWW-Authenticate': 'Basic realm ="Wrong Password !!"'})
    except Exception as e:
        app.logger.error(str(e))


@app.route(config.SLASH + config.USER_SIGNUP_ENDPOINT_STRING, methods=['POST'],
           endpoint=config.USER_SIGNUP_ENDPOINT_STRING)
def signup():
    try:
        # gets name, email and password
        email = request.json["email"]
        password = request.json["password"]
        name = request.json['name']
        role_name = str(request.json['role'])
        isSignedUp = LoggedInUserHelper.user_sign_up(email, password, name, role_name)
        if (isSignedUp):
            return make_response('Successfully registered.', 201)
        else:
            # returns 202 if user already exists
            return make_response('User already exists. Please Log in.', 202)
    except Exception as e:  
        app.logger.error(str(e))

@app.route(config.SLASH + config.USER_FORGOT_PASSWORD_ENDPOINT_STRING, methods=['POST'], endpoint=config.USER_FORGOT_PASSWORD_ENDPOINT_STRING)
def forgot_password():
    return


@app.route(config.SLASH + config.GET_AIR_SENSOR_DATA_STRING, methods=['GET'], endpoint=config.GET_AIR_SENSOR_DATA_STRING)
def get_senor_data():
    try:
        sensorname = request.args.get('sensorname')
        response = AirPollutionDataHelper.get_sensor_data_from_data_base(sensorname)
        if not response:
            return make_response('Please Chcek The Sensor Name', 201)
        else:
            return response
    except Exception as e:
        app.logger.error(str(e))
    
@app.route(config.SLASH+config.GET_AIR_SENSOR_PREDICT_ENDPOINT_STRING,methods=['GET'],endpoint=config.GET_AIR_SENSOR_PREDICT_ENDPOINT_STRING)
def get_predicted_value():
    try:
    # 'year','month','day','hour','minute
        sensorname = request.args.get('sensorname')
        year = int(request.args.get('year'))
        month = int(request.args.get('month'))
        date = int(request.args.get('day'))
        hour = int(request.args.get('hour'))
        minute = int(request.args.get('minute'))
        response = AirPollutionDataHelper.get_sensor_predicted_sata(sensorname, year, month, date, hour, minute)
        if(response==-1):
            return make_response('Prediction Not found', 404)
        return response
    except Exception as e:
        app.logger.error(str(e))



@app.route(config.SLASH+config.GET_ALL_AIR_SENSOR_DETAILS_ENDPOINT_STRING, methods=['GET'], endpoint=config.GET_ALL_AIR_SENSOR_DETAILS_ENDPOINT_STRING)
def get_all_sensor_details():
    try:
        response=AirPollutionDataHelper.get_all_sensor_data()
        response=response.to_json(orient='records')
        return response
    except Exception as e:
        app.logger.error(str(e))

@app.route(config.SLASH + config.REROUTE_DATA_ENDPOINT)
def get_reroute_data():
    device_id = request.args.get(config.REROUTE_DEVICEID_URL_PARAM)
    response= RerouteHelper.read_rerouting_pickle(device_id)
    return response

@app.route(config.SLASH + config.REROUTE_POLYGON_ENDPOINT)
def get_reroute_polygon():
    response= ReroutePolygon.get_polygons()
    return response





@app.route(config.SLASH + config.BUS_OPTIONS_SRTING, methods=['GET'])
def get_bus_options():
    return BusHelper().bus_route_dropdown_options_json()


@app.route(config.SLASH + config.BUS_TRIP_ID_STRING, methods=['GET'])
def send_bus_options():
    agency_id = request.args.get("agency_id")
    route_id = request.args.get("route_id")
    direction_id = request.args.get("d_id")
    return BusHelper().send_trip_id_json(agency_id, route_id, direction_id)


@app.route(config.SLASH + config.BUS_SHAPES_STRING, methods=['GET'])
def send_shape_stop_json():
    selected_trip_id = request.args.get("trip_id")
    return BusHelper().send_shapes_stops_realtime_json(selected_trip_id)


if __name__ == "__main__":
    app.run(threaded=True)
