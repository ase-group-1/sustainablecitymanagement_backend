import urllib.request, json 
import pandas as pd
import numpy as np
from pandas.io.json import json_normalize
import config
from DubBikes_ML_Engine.Prediction_Engine.ML_Prediction_Engine import ml_model_load
import requests
import xmltodict
from datetime import datetime
import pickle
from sklearn import preprocessing

import warnings
warnings.filterwarnings("ignore")

with urllib.request.urlopen(config.DB_BIKE_DATA_FETCH) as url:
    data = json.loads(url.read().decode())
    df = pd.DataFrame(json_normalize(data))
print(df.head())
def transform_data(df):
    df=df[["hrvst_ts","lat","longi","avl_bks"]]
    df["hrvst_ts"] = pd.to_datetime(df["hrvst_ts"])
    df.index = df["hrvst_ts"]
    df["year"] = df.index.year
    df["month"] = df.index.month
    df["day"] = df.index.day
    df["hour"] = df.index.hour
    df["minute"] = df.index.minute
    df["lat"]=df["lat"].astype(float)
    df["longi"]=df["longi"].astype(float)
    df.drop(["hrvst_ts"],axis=1,inplace=True)
    # df.rename(columns={"longi":"long"},inplace=True)
    df.reset_index(drop=True,inplace=True)
    return df

df = transform_data(df)
print(df.head())
df.rename(columns={"longi":"long"},inplace=True)
def get_weather_data(df):
    weather_data = pd.DataFrame(columns=["day","month","year","hour","minute","lat","long","temp"])
    for idx,row in df.iterrows():

        url_str = 'http://metwdb-openaccess.ichec.ie/metno-wdb2ts/locationforecast?lat='+str(row["lat"])+';long='+str(row["long"])
        response_API = requests.get(url_str)
        data = response_API.text
        data_dict = xmltodict.parse(data)
        json_data = json.dumps(data_dict)
        json_dict = json.loads(json_data)
        weather_test = json_normalize(json_dict) 
        weather_df = weather_test.explode('weatherdata.meta.model')
        weather_df_new = weather_df.explode('weatherdata.product.time')
        _LEN = len(dict(weather_test)['weatherdata.product.time'][0])
        # print(dict(weather_test)['weatherdata.product.time'][0][0])
        
        #Creating a new dataframe to store the weather data
        new_test = pd.DataFrame()
        new_test['day'] = [np.nan]
        new_test['month'] = [np.nan]
        new_test['year'] = [np.nan]
        new_test['hour'] = [np.nan]
        new_test['minute'] = [np.nan]
        new_test['lat'] = [np.nan]
        new_test['long'] = [np.nan]
        new_test['temp'] = [np.nan]
    
        date_time_obj = datetime.strptime(dict(weather_test)['weatherdata.product.time'][0][0]['@from'],"%Y-%m-%dT%H:%M:%SZ")
        new_test['day'][0] = date_time_obj.day
        new_test['month'][0] = date_time_obj.month
        new_test['year'][0] = date_time_obj.year
        new_test['hour'][0] = date_time_obj.hour
        new_test['minute'][0] = date_time_obj.minute
        new_test['lat'][0] = row["lat"]
        new_test['long'][0] = row["long"]
        if 'temperature' in dict(weather_test)['weatherdata.product.time'][0][0]['location'].keys():
            new_test['temp'][0] = dict(weather_test)['weatherdata.product.time'][0][0]['location']['temperature']['@value']
        new_test = new_test.dropna()
        new_test = new_test.reset_index(drop=True)
        weather_data=pd.concat([weather_data,new_test],axis=0)
    weather_data = weather_data[["temp","lat","long"]]
    return weather_data
    # return df
weather = get_weather_data(df)
print(weather)
df_combined = pd.merge(df, weather, on=['lat','long'])
print(df_combined)
#['year','month','day','hour','minute','temp','LATITUDE','LONGITUDE','AVAILABLE BIKES']
df_combined = df_combined[["year","month","day","hour","minute","temp","lat","long","avl_bks"]]
classifier = pickle.load(open('DubBikes_ML_Engine\gbr.pkl', 'rb'))
print(classifier.predict(df_combined))
# print(df.columns)