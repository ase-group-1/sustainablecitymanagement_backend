#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 10 10:46:05 2023

@author: abhik_bhattacharjee
"""

import urllib.request, json 
import pandas as pd
import numpy as np
from pandas.io.json import json_normalize
import config
from DubBikes_ML_Engine.Prediction_Engine.ML_Prediction_Engine import ml_model_load
import requests
import xmltodict
from datetime import datetime
import pickle
from sklearn import preprocessing

import warnings
warnings.filterwarnings("ignore")

with urllib.request.urlopen(config.DB_BIKE_DATA_FETCH) as url:
    data = json.loads(url.read().decode())
    df = pd.DataFrame(json_normalize(data))


file_nm = 'Resources/MLP_DubBikes.pkl'
ml_model_load().check_model_exist(file_nm)
classifier = ml_model_load().load_model(file_nm)

df = df.dropna()
df = df[df['sta'] == 'OPEN']
df['ls_updt_ts'] = pd.to_datetime(df.ls_updt_ts)
df['DAY_NUMBER'] = df.ls_updt_ts.dt.dayofweek
df['DAY_TYPE'] = np.where(df['DAY_NUMBER'] <= 4, 'Weekday', 
                            (np.where(df['DAY_NUMBER'] == 5, 'Saturday', 'Sunday')))
df['HOUR'] = df['ls_updt_ts'].dt.hour
df['MONTH'] = df['ls_updt_ts'].dt.month
df['date_for_merge'] = df['ls_updt_ts'].dt.round('H')

df_new = pd.DataFrame()
df_new['id'] = df['id']
df_new['addr_ln'] = df['addr_ln']
df_new['lat'] = df['lat']
df_new['longi'] = df['longi']
df_new['day_typ'] = df['DAY_TYPE']
df_new['day_nm'] = df['DAY_NUMBER']

df_new['day'] = [np.nan for i in range(len(df_new['id']))]
df_new['month'] = [np.nan for i in range(len(df_new['id']))]
df_new['year'] = [np.nan for i in range(len(df_new['id']))]
df_new['hour'] = [np.nan for i in range(len(df_new['id']))]
df_new['temp'] = [np.nan for i in range(len(df_new['id']))]
df_new['dewtp'] = [np.nan for i in range(len(df_new['id']))]
df_new['avl_bks'] = [np.nan for i in range(len(df_new['id']))]

for j in range(len(df_new.id.unique())):
    url_str = 'http://metwdb-openaccess.ichec.ie/metno-wdb2ts/locationforecast?lat='+str(df_new['lat'][j])+';long='+str(df_new['longi'][j])

    response_API = requests.get(url_str)
    data = response_API.text
    data_dict = xmltodict.parse(data)
    json_data = json.dumps(data_dict)

    json_dict = json.loads(json_data)

    weather_test = json_normalize(json_dict) 
    weather_df = weather_test.explode('weatherdata.meta.model')
    weather_df_new = weather_df.explode('weatherdata.product.time')

    new_test = pd.DataFrame()

    _LEN = len(dict(weather_test)['weatherdata.product.time'][0])

    new_test['day'] = [np.nan for i in range(_LEN)]
    new_test['month'] = [np.nan for i in range(_LEN)]
    new_test['year'] = [np.nan for i in range(_LEN)]
    new_test['hour'] = [np.nan for i in range(_LEN)]
    new_test['lat'] = [np.nan for i in range(_LEN)]
    new_test['long'] = [np.nan for i in range(_LEN)]
    new_test['temp'] = [np.nan for i in range(_LEN)]
    new_test['dewtp'] = [np.nan for i in range(_LEN)]
    
   

    for i in range(_LEN):
        date_time_obj = datetime.strptime(dict(weather_test)['weatherdata.product.time'][0][i]['@from'],"%Y-%m-%dT%H:%M:%SZ")
        new_test['day'][i] = date_time_obj.day
        new_test['month'][i] = date_time_obj.month
        new_test['year'][i] = date_time_obj.year
        new_test['hour'][i] = date_time_obj.hour
        new_test['lat'][i] = dict(weather_test)['weatherdata.product.time'][0][i]['location']['@latitude']
        new_test['long'][i] = dict(weather_test)['weatherdata.product.time'][0][i]['location']['@longitude']
        if 'temperature' in dict(weather_test)['weatherdata.product.time'][0][i]['location'].keys():
            new_test['temp'][i] = dict(weather_test)['weatherdata.product.time'][0][i]['location']['temperature']['@value']
        if 'dewpointTemperature' in dict(weather_test)['weatherdata.product.time'][0][i]['location'].keys():
            new_test['dewtp'][i] = dict(weather_test)['weatherdata.product.time'][0][i]['location']['dewpointTemperature']['@value']

    new_test = new_test.dropna()

    new_test = new_test.where((new_test.hour == datetime.today().hour + 1) & (new_test.day == datetime.today().day) 
                & (new_test.month == datetime.today().month) & (new_test.year == datetime.today().year))

    new_test = new_test.dropna()

    df_new['day'][j] = new_test['day'][0]
    df_new['month'][j] = new_test['month'][0]
    df_new['year'][j] = new_test['year'][0]
    df_new['hour'][j] = new_test['hour'][0]
    df_new['temp'][j] = new_test['temp'][0]
    df_new['dewtp'][j] = new_test['dewtp'][0]

label_encoder = preprocessing.LabelEncoder()
df_new['addr_ln'] = label_encoder.fit_transform(df_new['addr_ln'])
df_new['day_typ'] = label_encoder.fit_transform(df_new['day_typ'])

df_new['dewtp'] = pd.to_numeric(df_new['dewtp'])
df_new['temp'] = pd.to_numeric(df_new['temp'])
df_new['lat'] = pd.to_numeric(df_new['lat'])
df_new['longi'] = pd.to_numeric(df_new['longi'])

def z_score(x):
    return (x - x.mean())/(x.std())
scaling_data = ['addr_ln', 'lat', 'longi', 'day_nm', 
                       'temp', 'dewtp']
for i in scaling_data:
    df_new[i] = z_score(df_new[i])

classifier = pickle.load(open('Resources/MLP_DubBikes.pkl', 'rb'))

for k in range(len(df_new.id.unique())):
    test = [[df_new['id'][k], df_new['lat'][k],
         df_new['addr_ln'][k], df_new['longi'][k],
         df_new['day_nm'][k], df_new['day_typ'][k],
         df_new['hour'][k], df_new['month'][k],
         df_new['temp'][k], df_new['dewtp'][k]]]
    df_new['avl_bks'][k] = classifier.predict(test)
